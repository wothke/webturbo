I tried to modify the original source code of the mednafen emulator as little as possible (see 
EMSCRIPTEN comments).

The files contain add-on driver/glue code designed to feed music files into the emulation. These are 
add-on files not part of the original mednafen code base. But they are based on existing 3rd party 
code that I again tried to touch as little as possible.

In the case of mednafen_light.cpp it is basically a heavily stripped down version of the respective
original mednafen.cpp implementation (with non-music-related stuff thrown out). The
wsr_player.cpp (used for WonderSwan playback) is from the "foo_input_wsr" foobar2000 plugin. 