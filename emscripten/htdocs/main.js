let songs = [
		"WonderSwan/- unknown/arc the lad.wsr",
		"HES/Yuriko Keino/xevious.hes;2;7",
		"HES/Yuriko Keino/xevious.hes;3;11",
		"HES/- unknown/pastel lime.hes;4;20",
		"HES/Hiroshi Kawaguchi/out run.hes;3;87",
		"HES/Nobuyuki Ohnogi/galaga 88.hes;10;42",
		"HES/- unknown/pastel lime.hes;3;114",
		"HES/Jin Watanabe/battle lode runner.hes;2;60",
	];

function toHTML(txt)
{
	return txt.replaceAll("\n", "<br/>").replaceAll(" ", "&nbsp;");
}

class HESDisplayAccessor extends DisplayAccessor {
	constructor(doGetSongInfo)
	{
		super(doGetSongInfo);
	}

	getDisplayTitle() 		{ return "webTurbo";}
	getDisplaySubtitle() 	{ return "mednafen powered music";}
	getDisplayLine1() { return this.getSongInfo().title;}
	getDisplayLine2() { return this.getSongInfo().track.length? ("track "+this.getSongInfo().track):""; }
	getDisplayLine3() { return ""; }
};


class Main {
	constructor()
	{
		this._backend;
		this._playerWidget;
		this._songDisplay;
	}

	_doOnUpdate()
	{
		if (typeof this._lastId != 'undefined')
		{
			window.cancelAnimationFrame(this._lastId);	// prevent duplicate chains
		}
		this._animate();

		this._songDisplay.redrawSongInfo();
	}

	_animate()
	{
		this._songDisplay.redrawSpectrum();
		this._playerWidget.animate()

		this._lastId = window.requestAnimationFrame(this._animate.bind(this));
	}

	_doOnTrackEnd()
	{
		this._playerWidget.playNextSong();
	}

	_playSongIdx(i)
	{
		this._playerWidget.playSongIdx(i);
	}

	run()
	{
		let preloadFiles = [];	// no need for preload

		// note: with WASM this may not be immediately ready
		this._backend = new HESBackendAdapter();

		ScriptNodePlayer.initialize(this._backend, this._doOnTrackEnd.bind(this), preloadFiles, true, undefined)
		.then((msg) => {

			let makeOptionsFromUrl = function(someSong) {

					let arr = someSong.split(";");
					let track = arr.length > 1 ? parseInt(arr[1]) : -1;
					let timeout = arr.length > 2 ? parseInt(arr[2]) : -1;

					someSong = arr[0];

					let options = {};
					options.track = isNaN(track) ? -1 : track;
					options.timeout = isNaN(timeout) ? -1 : timeout;

					// drag&dropped temp files start with "/tmp/"
					let isLocal = someSong.startsWith("/tmp/") || someSong.startsWith("music/");
					someSong = isLocal ? someSong : window.location.protocol + "//ftp.modland.com/pub/modules/" + someSong;

					return [someSong, options];
				};

			this._playerWidget = new BasicControls("controls", songs, makeOptionsFromUrl, this._doOnUpdate.bind(this), false, true);

			this._songDisplay = new SongDisplay(new HESDisplayAccessor((function(){return this._playerWidget.getSongInfo();}.bind(this) )),
								[0x505050,0xffffff,0x404040,0xffffff], 1, 0.5);

			this._playerWidget.playNextSong();
		});
	}
}