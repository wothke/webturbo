/*
* This file adapts "mednafen" (PCE/WSWAN only) to the interface expected by my generic JavaScript player..
*
* Copyright (C) 2018 Juergen Wothke
*
* LICENSE
*
* This library is free software; you can redistribute it and/or modify it
* under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2.1 of the License, or (at
* your option) any later version. This library is distributed in the hope
* that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
* warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301 USA
*/

#include <emscripten.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h> /* strrchr */

#include <iostream>
#include <fstream>

#include <mednafen/mednafen.h>
#include <mednafen/git.h>
#include <pce/hes.h>

#include "MetaInfoHelper.h"

using emsutil::MetaInfoHelper;

// it does not seem that the zlib stuff is actually used in the .hes files scenario..
// but if I am wrong just remove the -USE_NULL_ZLIB from the make script
#ifdef USE_NULL_ZLIB
#define gzFile void*
#define voidp void*
#ifndef z_off_t
#  define z_off_t long
#endif
#ifndef OF /* function prototypes */
#  ifdef STDC
#    define OF(args)  args
#  else
#    define OF(args)  ()
#  endif
#endif

extern "C" {
	int     gzclose OF((gzFile file))							{ return 0;}
	gzFile  gzopen  OF((const char *path, const char *mode))	{ return NULL;}
	int  	gzeof OF((gzFile file))								{ return 0;}
	const char *  gzerror OF((gzFile file, int *errnum))		{ return NULL;}
	int     gzflush OF((gzFile file, int flush))				{ return 0;}
	int     gzgetc OF((gzFile file))							{ return 0;}
	int     gzread  OF((gzFile file, voidp buf, unsigned len))	{ return 0;}
	z_off_t	gzseek OF((gzFile file,z_off_t offset, int whence))	{ return 0;}
	z_off_t	gztell OF((gzFile file))							{ return 0;}
	int		gzwrite OF((gzFile file, voidpc buf, unsigned len))	{ return 0;}
	gzFile	gzdopen  OF((int fd, const char *mode))				{ return NULL;}
}
#endif

// HES APIs
extern void MDFN_IEN_PCE::HES_SetTrack(uint8 track);
extern uint8 MDFN_IEN_PCE::HES_GetTrack();

// WonderSwan APIs (from wsr_player.cpp)
extern int LoadWSR(const void* pFile, unsigned Size);
extern void CloseWSR();
extern int GetFirstSong(void);
extern unsigned SetFrequency(unsigned int Freq);
extern void ResetWSR(unsigned SongNo);
extern void FlushBufferWSR(const short * finalWave, unsigned long length);
extern int UpdateWSR(void* pBuf, unsigned Buflen, unsigned Samples);


// see Sound::Sample::CHANNELS
#define CHANNELS 2
#define BYTES_PER_SAMPLE 2
#define SAMPLE_BUF_SIZE	1024

struct SoftFB {
	std::unique_ptr<MDFN_Surface> surface = nullptr;
	MDFN_Rect rect;
	std::unique_ptr<int32[]> lw = nullptr;
	int field = -1;
};

namespace mdfn {

class Adapter {
public:
	Adapter()
	{
		_isWonderSwan = _songPlaying = false;
		_silenceTimeout = 10;
		_wsrBuffer = 0;
		_samplesAvailable= 0;
	}

	int loadFile(char *filename, void * inBuffer, uint32_t inBufSize, uint32_t sampleRate, uint32_t audioBufSize, uint32_t scopesEnabled)
	{
		// fixme: sampleRate, audioBufSize, scopesEnabled handling not implemented

		const char *ext = getFilenameExt(filename);
		_isWonderSwan = !strcmp(ext, "wsr") || !strcmp(ext, "WSR");

		_songPlaying = false;

		if(_isWonderSwan)
		{
			return loadWonderSwan(std::string(filename));
		}
		else
		{
			if (!setupEnvHES()) return 1;

			if(MDFNI_LoadGame(NULL, filename, false) != NULL)
			{
				setTitleInfo(filename);
				setHESTrackInfo();
				return 0;
			}
		}
		return 1;
	}

	int selectTrack(int id)
	{
		/* .hes files to contain multiple tracks but used IDs are not obvious:
		HES files are garbage: there are track-ids but within the range of 0-255
		used track-ids may be hidden anywhere. The "default" track-id
		defined for a song sometimes points to nothing so that drag&drop does not
		work to play anything audible.
		*/

		if (_isWonderSwan)
		{
			if (id<0)
			{
				id =  GetFirstSong();
			}
			ResetWSR(id);
		}
		else
		{
			if (id > 0)
			{
				MDFN_IEN_PCE::HES_SetTrack((uint8)id - 1);
				
				setHESTrackInfo();
			}
		}
		return 0;
	}

	int getSampleRate()
	{
		return _isWonderSwan ? 44100 : _espec.SoundRate;
	}

	int16_t *getSampleBuffer()
	{
		return _sampleBuffer;
	}

	void setSamplesAvailable(int a)
	{
		_samplesAvailable = a;
	}

	int getSamplesAvailable()
	{
		return _samplesAvailable;
	}

	int detectEnd()
	{
		if (_songPlaying)
		{
			bool silence = true;
			for (int i = 0; i<_samplesAvailable; i++) {
				if (((int*)_sampleBuffer)[i])
				{
					silence = false;
					break;
				}
			}
			if (silence && (_silenceTimeout-- < 1))
			{
				_samplesAvailable = 0; // end song
			}
		}
		else
		{
			for (int i = 0; i<_samplesAvailable; i++) {
				if (((int*)_sampleBuffer)[i])
				{
					_songPlaying = true;
					_silenceTimeout = 10;
					break;
				}
			}
		}
		return !(_samplesAvailable > 0);
	}

	int genSamples()
	{
		if (_isWonderSwan)
		{
			int bytes= UpdateWSR((void*) _sampleBuffer, 0, SAMPLE_BUF_SIZE);
			_samplesAvailable = bytes >> 2;

		}
		else
		{
			/* Emulates a frame. */
			MDFNI_Emulate(&_espec);	// triggers "Player_Draw"
		}
		return detectEnd();
	}

private:
	void initEmulator()
	{
		// todo: unify HES and WonderSwan handlung

		bool softFB_BackBuffer = false;	// garbage copied from original code

		// below settings may not be the absolute minimum.. but they work and I did not waste time to check..
		MDFN_PixelFormat nf(MDFN_COLORSPACE_RGB, 0, 8, 16, 24);
		_softFB[0].surface.reset(new MDFN_Surface(NULL, 1365, 270, 1365, nf));	// 1365: Framebuffer width, 270: Framebuffer height
		_softFB[0].lw.reset(new int32[270]);

		_softFB[0].surface->Fill(0, 0, 0, 0);

		_softFB[0].rect.w = std::min<int32>(16, _softFB[0].surface->w);
		_softFB[0].rect.h = std::min<int32>(16, _softFB[0].surface->h);
		_softFB[0].lw[0] = ~0;
		_softFB[softFB_BackBuffer].field = -1;;

		memset(&_espec, 0, sizeof(EmulateSpecStruct));

		_espec.surface = _softFB[softFB_BackBuffer].surface.get();
		_espec.LineWidths = _softFB[softFB_BackBuffer].lw.get();
		_espec.skip = 0;;
		_espec.soundmultiplier = 1;;
		_espec.NeedRewind = 0;;

		_espec.SoundRate = 44100;
		_espec.SoundBuf = _sampleBuffer;
		_espec.SoundBufMaxSize = SAMPLE_BUF_SIZE * CHANNELS;
		_espec.SoundVolume = 1;
	}


	void setTitleInfo(const char *songmodule)
	{
		MetaInfoHelper *info = MetaInfoHelper::getInstance();

			// poor man's song name.. these formats have no meta info...
		std::string displayname = std::string(songmodule);
//		displayname.erase( displayname.find_last_of( '.' ) );	// remove ext

		size_t n =  displayname.find_last_of ('/');	// remove path
		displayname = displayname.substr((n<displayname.length())? n+1 : 0);

		info->setText(0, displayname.c_str(), "");
	}

	void setHESTrackInfo()
	{
		uint8 track = MDFN_IEN_PCE::HES_GetTrack();	// read default
		char num[5];
		snprintf(num, 5, "%d", track);

		MetaInfoHelper *info = MetaInfoHelper::getInstance();
		info->setText(1, num, 0);
	}

	const char *getFilenameExt(const char *filename)
	{
		const char *dot = strrchr(filename, '.');
		if(!dot || dot == filename)
		{
			return "";
		}
		return dot + 1;
	}

	size_t getFileSize( FILE * f)
	{
		int fd = fileno(f);
		struct stat buf;
		fstat(fd, &buf);
		return buf.st_size;
	}

	int loadWonderSwan(std::string file)
	{
		FILE * f= fopen(file.c_str(), "r");
		size_t length = getFileSize( f );

		if (_wsrBuffer) {
			free((void*)_wsrBuffer);
		}
		_wsrBuffer = (uint8_t*)malloc( length );

		fread((void*)_wsrBuffer, 1, length, f );
		fclose( f );

		if(!LoadWSR((const void*) _wsrBuffer, length))
		{
			return 1;	// error
		}

		SetFrequency(44100);
		setTitleInfo(file.c_str());

		return 0;
	}

	int setupEnvHES()
	{
		initEmulator();

		// Call this function as early as possible, even before MDFNI_Initialize()
		if (MDFNI_InitializeModules())
		{
			if (MDFNI_Initialize("", _driverSettings))
			{	// FIXME check it this works without config files
				if ( MDFNI_LoadSettings("") != 0)
				{
					return 1;
				}
			}
		}
		return 0;
	}

private:
	bool _isWonderSwan;
	bool _songPlaying;
	int _silenceTimeout;

	int16_t _sampleBuffer[SAMPLE_BUF_SIZE * CHANNELS];
	int _samplesAvailable;

		// HES specific

	EmulateSpecStruct _espec;
	struct SoftFB _softFB[2];
	const std::vector<MDFNSetting> _driverSettings;

		// WonderSwan specific

	uint8_t *_wsrBuffer;
};


};

static mdfn::Adapter _adapter;


// "player callbacks" used by the emulator's HES_Update call
void Player_Init(int tsongs, const std::string &album, const std::string &artist,
	const std::string &copyright, const std::vector<std::string> &snames = std::vector<std::string>(),
	bool override_gi = true)
{
	// note: the info passed here seems to be pretty useless.. tsongs is always 256 and the remaining infos are empty
	// judging by the output of other players the "rom name" seems to be the most useful thing to display..
}

void Player_Draw(MDFN_Surface *surface, MDFN_Rect *dr, int track, int16 *samples, int32 sampcount) {
	if (samples != _adapter.getSampleBuffer())
	{
		fprintf(stderr, "ERRROR: invalid assumption\n");
	}
	else
	{
		_adapter.setSamplesAvailable(sampcount);
	}
}


// old style EMSCRIPTEN C function export to JavaScript.
// todo: code might be cleaned up using EMSCRIPTEN's "new" Embind feature:
// https://emscripten.org/docs/porting/connecting_cpp_and_javascript/embind.html
#define EMBIND(retType, func)  \
	extern "C" retType func __attribute__((noinline)); \
	extern "C" retType EMSCRIPTEN_KEEPALIVE func

EMBIND(int, emu_load_file(char *filename, void * inBuffer, uint32_t inBufSize, uint32_t sampleRate, uint32_t audioBufSize, uint32_t scopesEnabled)) {
	return _adapter.loadFile(filename, inBuffer, inBufSize, sampleRate, audioBufSize, scopesEnabled); }
EMBIND(void, emu_teardown())				{ MetaInfoHelper::getInstance()->clear(); }
EMBIND(int, emu_get_sample_rate())			{ return _adapter.getSampleRate(); }
EMBIND(int, emu_set_subsong(int subsong))	{ return _adapter.selectTrack(subsong); }
EMBIND(const char**, emu_get_track_info())	{ return MetaInfoHelper::getInstance()->getMeta(); }
EMBIND(char*, emu_get_audio_buffer())		{ return (char*)_adapter.getSampleBuffer(); }
EMBIND(long, emu_get_audio_buffer_length())	{ return _adapter.getSamplesAvailable(); }
EMBIND(int, emu_compute_audio_samples())	{ return _adapter.genSamples(); }
EMBIND(int, emu_get_current_position())		{ return 0; }
EMBIND(void, emu_seek_position(int pos))	{}
EMBIND(int, emu_get_max_position()) 		{ return 0; }
